#########################################################################
## Define your tables below; for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################

db.define_table('computers',
               Field('computer_index_id', required=True, unique=True),
               Field('computer_id'),
               Field('status',default="Unprocessed"),
               Field('Windows_COA_new'),
               Field('Windows_COA_old'),
               Field('Office_COA_new'),
               Field('Office_COA_old'),
               Field('OEM_SERIAL'),
               Field('Hardware_Manifest', 'upload', autodelete=True, uploadseparate=True),
               Field('comments','text'),
               Field('created_on','datetime', default=request.now, writable=False),
               Field('last_modified','datetime', default=request.now, update=request.now),
               Field('last_updated_time'),
               )