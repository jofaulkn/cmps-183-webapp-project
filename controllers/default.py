# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
#########################################################################
@auth.requires_login()
def index():
    logger.info("Here we are, in the index controller.")
    return dict()

@auth.requires_login()
def view_computer():
    comp_id = request.args(0)
    rows = db(db.computers.computer_index_id == comp_id).select()
    file = ""
    id = ""
    for r in rows:
        row = r
        file = r.Hardware_Manifest
        id = r.computer_index_id
    logger.info("Here we are, in the view_computer controller.")
    return dict(comp_id=comp_id,computerFILE = file,comp_index=id)

@auth.requires_signature()
def load_computers():
    keyword = request.vars.keyword
    search_by = request.vars.search_by
    rows = db().select(db.computers.ALL, orderby =~ db.computers.last_modified)
    if search_by == "INDEX":
        rows = db(db.computers.computer_index_id == keyword).select()
    if search_by == "ID#":
        rows = db(db.computers.computer_id == keyword).select()
    if search_by == "status":
        rows = db(db.computers.status == keyword).select(orderby =~ db.computers.last_modified)
    if search_by == "WinCOA":
        rows = db((db.computers.Windows_COA_new == keyword) | (db.computers.Windows_COA_old == keyword)).select()
    if search_by == "OffCOA":
        rows = db((db.computers.Office_COA_new == keyword) | (db.computers.Office_COA_old == keyword)).select()
    if search_by == "OEM#":
        rows = db(db.computers.OEM_SERIAL == keyword).select()

    d = { r.computer_index_id : {'computer_index_id':r.computer_index_id,
                                 'computer_id':r.computer_id,
                                 'status': r.status,
                                 'Windows_COA_new':r.Windows_COA_new,
                                 'Windows_COA_old':r.Windows_COA_old,
                                 'Office_COA_new':r.Office_COA_new,
                                 'Office_COA_old':r.Office_COA_old,
                                 'OEM_SERIAL':r.OEM_SERIAL,
                                 #'Hardware_Manifest':r.Hardware_Manifest,
                                 'comments':r.comments,
                                 'created_on':r.created_on,
                                 'last_modified':r.last_modified}
          for r in rows}

    return response.json(dict(computer_dict=rows.as_list()))

@auth.requires_signature()
def add_or_update_computer():
    computer_index_id = request.vars.computer_index_id
    computer_id = request.vars.computer_id
    status = request.vars.status
    Windows_COA_new = request.vars.Windows_COA_new
    Windows_COA_old = request.vars.Windows_COA_old
    Office_COA_new = request.vars.Office_COA_new
    Office_COA_old = request.vars.Office_COA_old
    OEM_SERIAL = request.vars.OEM_SERIAL
    Hardware_Manifest = request.vars.Hardware_Manifest
    comments = request.vars.comments
    last_updated_time = request.vars.last_updated_time

    db.computers.update_or_insert(db.computers.computer_index_id == computer_index_id,
                                  computer_index_id =computer_index_id,
                                  computer_id = computer_id,
                                  status=status,
                                  Windows_COA_new = Windows_COA_new,
                                  Windows_COA_old = Windows_COA_old,
                                  Office_COA_new = Office_COA_new,
                                  Office_COA_old = Office_COA_old,
                                  OEM_SERIAL = OEM_SERIAL,
                                 #Hardware_Manifest = Hardware_Manifest,
                                  comments = comments,
                                  last_updated_time = last_updated_time,
                                  )
    return "ok"

def uploadmanifest():
    uploadfile = request.vars.manifestfile
    computerid = request.vars.computerid
    db.computers.update_or_insert(db.computers.computer_index_id == computerid,Hardware_Manifest = uploadfile,)

    return 'ok'

def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


